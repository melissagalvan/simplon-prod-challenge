# Simplon Prod Challenge

### Sommaire :

1. [Présentation du projet](#présentation-du-projet)
2. [Technologies utilisées](#technologies-utilisées)
3. [Interets](#interets)
4. [Installation et utilisation](#installation-et-utilisation)

### <span name="présentation-du-projet">Présentation du projet</span>
> Nous devions créer une API avec une technologie inconnue(Laravel) afin de juger notre capacité à<br>comprendre et à appliquer une documentation(avec la possibilité de checker des forums).<br>
> Pour cela, nous devions créer un model apprenant ainsi qu'un model promo et lier les deux.<br>
Puis créer une route qui servira à afficher toutes les promos et leurs apprenants ainsi qu'en bonus,<br>un moyen d'en ajouter un.

__*Temps*__
1 semaine

## <span name="technologies-utilisées">Technologies utilisées</span>

* PHP 
    * Laravel


## <span name="interets">Interets</span>

Ce challenge m'a permis de me familiariser avec une nouvelle technologie grâce à la documentation.

## <span name="installation-et-utilisation">Installation et utilisation</span>

1. __Cloner le projet__ 
2. Installer les dépendances avec __composer install__
3. Dans le fichier __.env__, entrer les informations de connexion à votre base de donnée.
```
DB_DATABASE=db_name
DB_USERNAME=username
DB_PASSWORD=password
```
4. Etablir les tables avec __php artisan migrate__.
5. Possibilité de charger les seeds avec __php artisan db:seed__.