<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('students')->insert([     // New student
            'name' => 'Keller',
            'surname' => 'Samuel',
            'email' => 'sam@email.com',
            'external_url' => 'http://sam.com',
            'promo_id' => 1
        ]);

        DB::table('students')->insert([     // New student
            'name' => 'Galvan',
            'surname' => 'Melissa',
            'email' => 'mel@email.com',
            'external_url' => 'http://mel.com',
            'promo_id' => 1
        ]);
    }
}
